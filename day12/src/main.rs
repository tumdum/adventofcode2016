#[macro_use]
extern crate lazy_static;
use regex::Regex;
use std::io::BufRead;
use std::ops::Deref;
use std::collections::HashMap;

lazy_static! {
    static ref CPY_RE : Regex = Regex::new(r"cpy (.+) (.+)").unwrap(); 
    static ref INC_RE : Regex = Regex::new(r"inc (.+)").unwrap(); 
    static ref DEC_RE : Regex = Regex::new(r"dec (.+)").unwrap(); 
    static ref JNZ_RE : Regex = Regex::new(r"jnz (.+) (.+)").unwrap(); 
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum Reg { A, B, C, D }
impl From<&str> for Reg {
    fn from(s: &str) -> Reg {
        match s {
            "a" => Reg::A,
            "b" => Reg::B,
            "c" => Reg::C,
            "d" => Reg::D,
            _ => panic!(),
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum Loc {
    Direct(i64),
    Register(Reg),
}
impl From<&str> for Loc {
    fn from(s: &str) -> Loc {
        match s.parse::<i64>() {
            Ok(v) => Loc::Direct(v),
            _ => Loc::Register(s.into()),
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum Op {
    Cpy{x: Loc, y: Reg},
    Inc{x: Reg},
    Dec{x: Reg},
    Jnz{x: Loc, y: i64},
}
impl From<&str> for Op {
    fn from(s: &str) -> Op {
        if let Some(cap) = CPY_RE.captures(s) {
            return Op::Cpy{x: cap[1].into(), y: cap[2].into()};
        }
        if let Some(cap) = INC_RE.captures(s) {
            return Op::Inc{x: cap[1].into()}
        }
        if let Some(cap) = DEC_RE.captures(s) {
            return Op::Dec{x: cap[1].into()}
        }
        if let Some(cap) = JNZ_RE.captures(s) {
            return Op::Jnz{x: cap[1].into(), y: cap[2].parse().unwrap()}
        }
        unimplemented!()
    }
}

#[test]
fn parse() {
    assert_eq!(Op::Cpy{x: Loc::Direct(123), y: Reg::B},
        "cpy 123 b".into());
    assert_eq!(Op::Inc{x: Reg::A}, "inc a".into());
    assert_eq!(Op::Dec{x: Reg::C}, "dec c".into());
    assert_eq!(Op::Jnz{x: Loc::Register(Reg::D), y: -54}, "jnz d -54".into());
}

fn value(state: &HashMap<Reg, i64>, l: &Loc) -> i64 {
    match l {
        Loc::Direct(v) => *v,
        Loc::Register(r) => state[r],
    }
}

fn run(ops: &Vec<Op>, c: i64) {
    let mut regs : HashMap<Reg, i64> = HashMap::new();
    regs.insert(Reg::A, 0);
    regs.insert(Reg::B, 0);
    regs.insert(Reg::C, c);
    regs.insert(Reg::D, 0);

    let mut ip : i64 = 0;
    loop {
        match &ops[ip as usize] {
            Op::Cpy{x, y} => { 
                regs.insert(y.clone(), value(&regs, &x)); 
                ip += 1;
            },
            Op::Inc{x} => {
                regs.insert(x.clone(), value(&regs, &Loc::Register(x.clone()))+1);
                ip += 1;
            },
            Op::Dec{x} => {
                regs.insert(x.clone(), value(&regs, &Loc::Register(x.clone()))-1);
                ip += 1;
            },
            Op::Jnz{x, y} => {
                let v = value(&regs, &x);
                if v != 0 {
                    ip += y;
                } else {
                    ip += 1;
                }
            }
            _ => unimplemented!(),
        }
        if ip < 0 || ip >= (ops.len() as i64) {
            break;
        }
    }
    println!("{:?}", regs);
}

fn main() {
    let parts : Vec<Op> = std::io::BufReader::new(std::io::stdin())
        .lines()
        .map(|s| s.unwrap())
        .map(|s| s.deref().into())
        .collect();
    run(&parts, 0);
    run(&parts, 1);
}
