use std::collections::{HashMap, HashSet};

#[derive(Debug, PartialEq)]
enum Type { Open, Wall }

enum Dir { Up, Down, Left, Right }

#[derive(Clone, Debug, PartialEq, Hash, Eq)]
struct Pos {
    x: usize,
    y: usize,
}

impl Pos {
    fn get_type(&self, fav: usize) -> Type {
        let v = self.x*self.x + 3*self.x + 2*self.x*self.y + self.y + self.y*self.y;
        let v = v + fav;
        let ones = v.count_ones();
        if ones % 2 == 0 {
            Type::Open
        } else {
            Type::Wall
        }
    }

    fn move_by(&self, dir: &Dir) -> Option<Pos> {
        match dir {
            Dir::Up => if self.y == 0 { None } else { Some(Pos{x: self.x, y: self.y-1}) }
            Dir::Down => Some(Pos{x: self.x, y: self.y+1}),
            Dir::Left => if self.x == 0 { None } else { Some(Pos{x: self.x-1, y: self.y}) }
            Dir::Right => Some(Pos{x: self.x+1, y: self.y}),
        }
    }

    fn possible_moves<Pred: Fn(&Pos)->bool>(&self, fav: usize, pred: Pred) -> HashSet<Pos> {
        [Dir::Up, Dir::Down, Dir::Left, Dir::Right]
            .iter()
            .flat_map(|d| self.move_by(d))
            .filter(|p| p.get_type(fav) == Type::Open)
            .filter(|p| pred(&p))
            .collect()
    }
}

#[test]
fn type_test() {
    let fav = 10;
    assert_eq!(Type::Open, Pos{x: 0, y: 0}.get_type(fav));
    assert_eq!(Type::Wall, Pos{x: 9, y: 6}.get_type(fav));
}

fn find_path(from: &Pos, to: &Pos, fav: usize) -> Option<Vec<Pos>> {
    let mut seen = HashSet::new();
    let mut todo = std::collections::VecDeque::new();
    let mut prev : HashMap<Pos, Pos> = HashMap::new();
    for p in from.possible_moves(fav, |_| true).iter() {
        prev.insert(p.clone(), from.clone());
        todo.push_back(p.clone());
    }
    assert!(!prev.contains_key(&from));
    let mut found = false;
    while !todo.is_empty() {
        let next = todo.pop_front().unwrap();
        if &next == to {
            found = true;
            break;
        }
        seen.insert(next.clone());
        for p in next.possible_moves(fav, |p| !seen.contains(p)).iter() {
            prev.insert(p.clone(), next.clone());
            todo.push_back(p.clone());
        }
    }
    if !found {
        return None
    }
    let mut path = vec![to.clone()];
    let mut current = to.clone();
    loop {
        if &current == from {
            break
        }
        current = prev[&current].clone();
        path.push(current.clone());
    }
    assert_eq!(path.iter().cloned().collect::<HashSet<_>>().len(), path.len());
    Some(path)
}
fn main() {
    let start = Pos{x: 1, y: 1};
    assert_eq!(12, find_path(&start, &Pos{x: 7, y: 4}, 10).unwrap().len());
    let path = find_path(&start, &Pos{x: 31, y: 39}, 1364).unwrap();
    println!("{}", path.len()-1);

    let mut count = 0;
    for row in 0..51 {
        for col in 0..51 {
            let dest = Pos{x: col, y: row};
            let path = find_path(&Pos{x: 1, y: 1}, &dest, 1364);
            if path.is_some() {
                let steps = path.unwrap().len() - 1;
                if steps <= 50 {
                    count += 1;
                }
            }
        }
    }
    println!("{}", count);
}
