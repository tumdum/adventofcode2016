use std::str::FromStr;
#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;
use std::io::stdin;
use std::io::Read;
use std::collections::HashMap;
use std::collections::HashSet;

const modulo: i32 = ('z' as i32) - ('a' as i32) + 1;

fn rot(c: char, n: i32) -> char {
    if c == '-' {
        return ' ';
    }
    (((c as i32 + n - 'a' as i32) % modulo) + 'a' as i32) as u8 as char
}

#[derive(Debug,PartialEq,Eq)]
struct Room {
    letters: String,
    sector_id: i32,
    checksum: String,
}

impl FromStr for Room {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new("([a-z-]+)([0-9]+)\\[([a-z]{5})\\]").unwrap();
        }
        let caps = RE.captures(s).unwrap();
        let sector_id: i32 = caps.at(2).unwrap().parse().unwrap();
        Ok(Room {
            letters: caps.at(1).unwrap().replace("-", ""),
            sector_id: sector_id,
            checksum: caps.at(3).unwrap().to_string(),
        })
    }
}

impl Room {
    fn verify(&self) -> bool {
        let mut histogram = HashMap::new();
        for c in self.letters.chars() {
            let stat = histogram.entry(c).or_insert(0);
            *stat += 1;
        }
        let mut freq = vec![];
        for (_, f) in &histogram {
            freq.push(f);
        }
        freq.sort();
        freq.reverse();
        let minAcceptable = freq[4];
        let mut required = HashSet::new();
        for (k, v) in &histogram {
            if v > minAcceptable {
                required.insert(k);
            }
        }
        for c in self.checksum.chars() {
            match histogram.get(&c) {
                Some(v) => {
                    if v < minAcceptable {
                        return false;
                    } else {
                        required.remove(&c);
                    }
                }
                None => return false,

            }
        }
        return required.is_empty();
    }

    fn decrypt(&self) -> String {
        self.letters.chars().map(|c| rot(c, self.sector_id)).collect()
    }
}

fn main() {
    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    let mut count = 0;
    for line in buf.lines() {
        let room: Room = line.parse().unwrap();
        if room.verify() {
            count += room.sector_id;
        }
        println!("{} -> {}", room.decrypt(), room.sector_id);
    }
    println!("{}", count);
}

static INPUT1: &'static str = "aaaaa-bbb-z-y-x-123[abxyz]";
static INPUT2: &'static str = "a-b-c-d-e-f-g-h-987[abcde]";
static INPUT3: &'static str = "not-a-real-room-404[oarel]";
static INPUT4: &'static str = "totally-real-room-200[decoy]";
static INPUT5: &'static str = "aabcdefghijk-100[bcdef]";

#[test]
fn test_from_str() {
    assert_eq!(Room {
                   letters: "aaaaabbbzyx".to_string(),
                   sector_id: 123,
                   checksum: "abxyz".to_string(),
               },
               INPUT1.parse().unwrap());
    assert_eq!(Room {
                   letters: "abcdefgh".to_string(),
                   sector_id: 987,
                   checksum: "abcde".to_string(),
               },
               INPUT2.parse().unwrap());
    assert_eq!(Room {
                   letters: "notarealroom".to_string(),
                   sector_id: 404,
                   checksum: "oarel".to_string(),
               },
               INPUT3.parse().unwrap());
    assert_eq!(Room {
                   letters: "totallyrealroom".to_string(),
                   sector_id: 200,
                   checksum: "decoy".to_string(),
               },
               INPUT4.parse().unwrap());
    assert_eq!(Room {
                   letters: "aabcdefghijk".to_string(),
                   sector_id: 100,
                   checksum: "bcdef".to_string(),
               },
               INPUT5.parse().unwrap());
}

#[test]
fn test_verify() {
    assert_eq!(true, INPUT1.parse::<Room>().unwrap().verify());
    assert_eq!(true, INPUT2.parse::<Room>().unwrap().verify());
    assert_eq!(true, INPUT3.parse::<Room>().unwrap().verify());
    assert_eq!(false, INPUT4.parse::<Room>().unwrap().verify());
    assert_eq!(false, INPUT5.parse::<Room>().unwrap().verify());
}

#[test]
fn test_decrypt() {
    assert_eq!('b', rot('a', 1));
    assert_eq!('c', rot('b', 1));
    assert_eq!('a', rot('z', 1));
    assert_eq!('v', rot('q', 343));
    assert_eq!(' ', rot('-', 343));
    let r = Room {
        letters: "qzmt-zixmtkozy-ivhz".to_string(),
        sector_id: 343,
        checksum: "notImportant".to_string(),
    };
    assert_eq!("very encrypted name", r.decrypt());
}
