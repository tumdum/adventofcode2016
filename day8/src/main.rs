#[macro_use]
extern crate lazy_static;

use std::io::Read;

#[derive(Debug, PartialEq)]
pub enum Dir { Row, Col }

impl From<&str> for Dir {
    fn from(s: &str) -> Dir {
        match s {
            "row" => Dir::Row,
            "column" => Dir::Col,
            _ => panic!(format!("unsupported '{}' dir", s)),
        }
    }
}

lazy_static! {
    static ref RECT_RE : regex::Regex =
        regex::Regex::new(r"rect (\d+)x(\d+)").unwrap();
    static ref ROTATE_RE : regex::Regex =
        regex::Regex::new(r"rotate (row|column) (?:x|y)=(\d+) by (\d+)").unwrap();
}

#[derive(Debug, PartialEq)]
pub enum Op {
    Rect{x: usize, y: usize},
    Rotate{dir: Dir, pos: usize, by: usize},
}

impl From<&str> for Op {
    fn from(s: &str) -> Op {
        if let Some(cap) = RECT_RE.captures(s) {
            let x = cap[1].parse().unwrap();
            let y = cap[2].parse().unwrap();
            return Op::Rect{x, y}
        }

        let cap = ROTATE_RE.captures(s).unwrap();
        let dir = cap[1].into();
        let pos = cap[2].parse().unwrap();
        let by  = cap[3].parse().unwrap();
        Op::Rotate{dir, pos, by}
    }
}

#[derive(Hash, PartialEq, Eq)]
pub struct Pos {
    x: usize,
    y: usize,
}

pub struct Board {
    data: Vec<Vec<bool>>,
}

impl Board {
    fn new(rows: usize, columns: usize) -> Self {
        Board{data: vec![vec![false; columns]; rows] }
    }
    fn apply(&mut self, op: &Op) {
        match op {
            Op::Rect{x, y} => {
                for col in 0..*x {
                    for row in 0..*y {
                        self.data[row][col] = true;
                    }
                }
            }
            Op::Rotate{dir: Dir::Col, pos, by} => {
                let mut new = self.get_col(*pos);
                new.rotate_right(*by);
                self.set_col(*pos, new);
            }
            Op::Rotate{dir: Dir::Row, pos, by} => {
                let mut new = self.get_row(*pos);
                new.rotate_right(*by);
                self.set_row(*pos, new);
            }
        }
    }

    fn get_col(&self, col: usize) -> Vec<bool> {
        let mut ret = vec![];
        for row in self.data.iter() {
            ret.push(row[col]);
        }
        return ret
    }

    fn get_row(&self, row: usize) -> Vec<bool> {
        self.data[row].clone()
    }

    fn set_col(&mut self, col: usize, new: Vec<bool>) {
        for (i, row) in self.data.iter_mut().enumerate() {
            row[col] = new[i];
        }
    }

    fn set_row(&mut self, row: usize, new: Vec<bool>) {
        self.data[row] = new;
    }

    fn lit(&self) -> usize {
        self.data
            .iter()
            .map(|r| r.iter().filter(|v| **v).count())
            .sum()
    }

    fn dump(&self) {
        for row in self.data.iter() {
            for col in row.iter() {
                match col {
                    true => print!("#"),
                    false => print!("."),
                }
            }
            println!();
        }
    }
}

#[test]
fn dir_from() {
    assert_eq!(Dir::Row, "row".into());
    assert_eq!(Dir::Col, "column".into());
}

#[test]
fn op_from() {
    assert_eq!(Op::Rect{x: 12, y: 9}, "rect 12x9".into());
    assert_eq!(Op::Rotate{dir: Dir::Row, pos: 7, by: 2}, "rotate row y=7 by 2".into());
}

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let commands = buf
        .split('\n')
        .filter(|s| s.len() > 0)
        .map(Op::from);
    let mut board = Board::new(6, 50);
    for c in commands {
        board.apply(&c);
    }
    println!("lit: {}", board.lit());
    board.dump();
}
