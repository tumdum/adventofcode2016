use std::io::stdin;
use std::io::Read;

use std::collections::HashSet;

fn is_tls(s: &str) -> bool {
    let bytes = s.to_string().into_bytes();
    let mut inside = false;
    let mut found = false;
    for w in bytes.windows(4) {
        if w[0] == '[' as u8 {
            inside = true;
        } else if w[0] == ']' as u8 {
            inside = false;
        }
        if w[0] != w[1] && w[0] == w[3] && w[1] == w[2] {
            if !inside {
                found = true;
            } else {
                return false;
            }
        }
    }
    found
}

fn is_ssl(s: &str) -> bool {
    let bytes = s.to_string().into_bytes();
    let mut aba = HashSet::new();
    let mut bab = HashSet::new();
    let mut inside = false;
    let mut found = false;

    for w in bytes.windows(3) {
        if w[0] == '[' as u8 {
            inside = true;
        } else if w[0] == ']' as u8 {
            inside = false;
        }
        if w[0] == w[2] && w[0] != w[1] {
            if !inside {
                aba.insert((w[0], w[1], w[2]));
                if bab.contains(&(w[1], w[0], w[1])) {
                    found = true;
                }
            } else {
                bab.insert((w[0], w[1], w[2]));
                if aba.contains(&(w[1], w[0], w[1])) {
                    found = true;
                }
            }
        }
    }
    
    found
}

fn main() {
    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    let count = buf.lines().map(|l| is_ssl(l)).filter(|s| *s).count();
    println!("{}", count);
}

#[test]
fn test_tls() {
    assert!(is_tls("abba[mnop]qrst"));
    assert!(!is_tls("abcd[bddb]xyyx"));
    assert!(!is_tls("aaaa[qwer]tyui"));
    assert!(is_tls("ioxxoj[asdfgh]zxcvbn"));
    assert!(!is_tls("abba[cddc]xaz"));
}

#[test]
fn test_ssl() {
    assert!(is_ssl("aba[bab]xyz"));
    assert!(!is_ssl("xyx[xyx]xyx"));
    assert!(is_ssl("aaa[kek]eke"));
    assert!(is_ssl("zazbz[bzb]cdb"));
}
