use std::io::BufRead;

#[derive(Clone, Debug)]
struct Range {
    start: u32,
    end: u32,
}

impl Range {
    fn is_in(&self, v: u32) -> bool {
        v >= self.start && v <= self.end
    }
}

fn parse(s: &str) -> Range {
    let mut nums = s.split('-')
        .map(|s| s.parse().unwrap());
    let start = nums.next().unwrap();
    let end = nums.next().unwrap();
    Range{start, end}
}

fn is_in(ranges: &[Range], value: u32) -> bool {
    for r in ranges {
        if r.is_in(value) {
            return true
        }
    }
    false
}

fn merge(ranges: &[Range]) -> Vec<Range> {
    let mut ret = vec![ranges[0].clone()];
    for r in ranges.iter() {
        let mut last = ret.last_mut().unwrap();
        if last.is_in(r.start) {
            last.end = std::cmp::max(last.end, r.end);
        } else {
            ret.push(r.clone());
        }
    }
    ret
}

// part 2 - 266 is too high
//          112 is too low

fn main() {
    let mut ranges : Vec<_> = std::io::BufReader::new(std::io::stdin())
        .lines()
        .map(|s| parse(&s.unwrap()))
        .collect();
    ranges.sort_unstable_by_key(|r| r.start);
    let ranges = merge(&ranges);

    for i in u32::min_value()..=u32::max_value() {
        if !is_in(&ranges, i) {
            println!("part 1: {}", i);
            break;
        }
    }

    let mut start = 0;
    let mut count = 0;
    for range in ranges.iter() {
        let mut size = range.start - start;
        if size > 0 {
            size -= 1;
        }
        count += size;
        start = range.end;
    }
    count += u32::max_value() - start;
    println!("part 2: {}", count);
}
