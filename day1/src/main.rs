use std::io::Read;
use std::str::FromStr;
use std::collections::HashSet;
use std::clone::Clone;
use std::cmp;

#[derive(Debug)]
enum Rotation {
    Left,
    Right,
}

impl FromStr for Rotation {
    type Err = ();

    fn from_str(s: &str) -> Result<Rotation, Self::Err> {
        match s {
            "L" => Ok(Rotation::Left),
            "R" => Ok(Rotation::Right),
            _   => Err(()),
        }
    }
}


#[derive(Debug,Clone)]
enum Direction {
    North,
    West,
    South,
    East,
}

impl Direction {
    fn rotate_left(&self) -> Direction {
        match *self {
            Direction::North => Direction::West,
            Direction::West  => Direction::South,
            Direction::South => Direction::East,
            Direction::East  => Direction::North,
        }
    }

    fn rotate_right(&self) -> Direction {
        self.rotate_left().rotate_left().rotate_left()
    }

    fn rotate_by(&self, rot: &Rotation) -> Direction {
        match *rot {
            Rotation::Left  => self.rotate_left(),
            Rotation::Right => self.rotate_right(),
        }
    }
}

#[derive(Debug)]
struct Move {
    rot: Rotation,
    dist: i32,
}

impl FromStr for Move {
    type Err = ();
    
    fn from_str(s: &str) -> Result<Move, Self::Err> {
        let (rot, dist) = s.split_at(1);
        let rot : Rotation = rot.parse().unwrap();
        let dist : i32 = dist.parse().unwrap();
        Ok(Move{rot: rot, dist: dist})
    }
}

#[derive(Debug,Clone)]
struct Position {
    x: i32,
    y: i32,
    dir: Direction,
}

impl Position {

    fn rotate_and_move(&self, m: &Move) -> Position {
        let mut next = self.clone();
        next.dir = self.dir.rotate_by(&m.rot);
        next.move_by(m)
    }

    fn rotate(&self, m: &Move) -> Position {
        let mut next = self.clone();
        next.dir = self.dir.rotate_by(&m.rot);
        next
    }

    fn step(&self, dist: i32) -> Position {
        let mut next = self.clone();
        match self.dir {
            Direction::North => next.y += dist,
            Direction::West  => next.x -= dist,
            Direction::South => next.y -= dist,
            Direction::East  => next.x += dist,
        }
        next
    }
    
    fn move_by(&self, m: &Move) -> Position {
        self.step(m.dist)
    }

    fn dist(&self) -> i32 {
        return self.x.abs() + self.y.abs()
    }
}

fn main() {
    let mut input = String::new();
    std::io::stdin().read_to_string(&mut input).unwrap();

    let input = input.trim();
    println!("input: '{}'", input);

    let mut seen = HashSet::new();

    let split = input.split(", ");
    let mut pos = Position{x: 0, y: 0, dir: Direction::North};
    let mut ret : Option<Position> = None;
    seen.insert((pos.x,pos.y));
    'found: for x in split {
        let mov: Move = x.parse().unwrap();
        pos = pos.rotate(&mov);
        for i in 0..mov.dist {
            pos = pos.move_by(&Move{rot: Rotation::Left, dist: 1});
            println!("next: {:?}", pos);
            if seen.contains(&(pos.x, pos.y)) {
                ret = Some(pos);
                break 'found;
            } else {
                seen.insert((pos.x, pos.y));
            }
        }
    }
    println!("{:?}", ret.map(|d| d.dist()));
}
