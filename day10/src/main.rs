#[macro_use]
extern crate lazy_static;
use regex::Regex;
use std::io::Read;
use std::collections::{HashMap, HashSet};

lazy_static! {
    static ref VALUE_RE : Regex =
        Regex::new(r"value (\d+) goes to bot (\d+)").unwrap();
    static ref BOT_RE : Regex =
        Regex::new(r"bot (\d+) gives low to (bot|output) (\d+) and high to (bot|output) (\d+)").unwrap();
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
enum Dest {
    Bot(usize),
    Output(usize),
}

impl From<&str> for Dest {
    fn from(s: &str) -> Dest {
        Dest::Bot(s.parse().unwrap())
    }
}

#[derive(Debug, PartialEq)]
enum Instr {
    Value{which: usize, to: usize},
    Bot{which: usize, low: Dest, high: Dest},
}

impl From<&str> for Instr {
    fn from(s: &str) -> Instr {
        if let Some(cap) = VALUE_RE.captures(s) {
            return Instr::Value{
                which: cap[1].parse().unwrap(),
                to: cap[2].parse().unwrap(),
            };
        }
        let cap = BOT_RE.captures(s).unwrap();
        let low_id = cap[3].parse().unwrap();
        let high_id = cap[5].parse().unwrap();
        let low = if &cap[2] == "bot" {
            Dest::Bot(low_id)
        } else {
            Dest::Output(low_id)
        };
        let high = if &cap[4] == "bot" {
            Dest::Bot(high_id)
        } else {
            Dest::Output(high_id)
        };
        Instr::Bot{
            which: cap[1].parse().unwrap(),
            low: low,
            high: high,
        }
    }
}

#[test]
fn instr_from() {
    assert_eq!(Instr::Value{which:5, to:2}, "value 5 goes to bot 2".into());
    assert!(BOT_RE.is_match("bot 2 gives low to bot 1 and high to bot 0"));
    assert!(BOT_RE.is_match("bot 0 gives low to output 2 and high to output 0"));
    assert_eq!(Instr::Bot{which: 2, low: Dest::Bot(1), high: Dest::Bot(0)}, "bot 2 gives low to bot 1 and high to bot 0".into());
    assert_eq!(Instr::Bot{which: 0, low: Dest::Output(2), high: Dest::Output(0)}, "bot 0 gives low to output 2 and high to output 0".into());
}

fn add(state: &mut HashMap<Dest, HashSet<usize>>, where_to: &Dest, what: &usize) {
    state
        .entry(where_to.clone())
        .or_insert(HashSet::new())
        .insert(*what);
}

fn bot_id(d: &Dest) -> usize {
    match d {
        Dest::Bot(v) => *v,
        _ => panic!(),
    }
}

fn run_state(mut state: &mut HashMap<Dest, HashSet<usize>>,
    bots: &mut HashMap<(usize, usize), usize>,
    low: &HashMap<usize, Dest>,
    high: &HashMap<usize, Dest>) {
    loop {
        let mut found_too_big = None;
        for (output, values) in state.iter() {
            if values.len() <= 1 { 
                continue;
            }
            assert!(values.len() == 2);
            found_too_big = Some(output.clone());
            break;

        }
        if found_too_big.is_none() {
            break;
        }
        let mut sorted : Vec<_> = state[&found_too_big.clone().unwrap()].iter().cloned().collect();
        sorted.sort();
        let id = bot_id(&found_too_big.clone().unwrap());
        bots.insert((sorted[0], sorted[1]), id);
        let low_dest = low[&id].clone();
        let high_dest = high[&id].clone();
        add(&mut state, &low_dest, &sorted[0]);
        add(&mut state, &high_dest, &sorted[1]);
        state.get_mut(&found_too_big.unwrap()).map(|s| s.clear());
    }
}

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let commands : Vec<_> = buf
        .split('\n')
        .filter(|s| s.len() > 0)
        .map(Instr::from)
        .collect();
    let mut low_map : HashMap<usize, Dest> = HashMap::new();
    let mut high_map : HashMap<usize, Dest> = HashMap::new();
    let mut state : HashMap<Dest, HashSet<usize>> = HashMap::new();
    for i in commands.iter() {
        match i {
            Instr::Bot{which: w, low: l, high: h} => {
                low_map.insert(*w, l.clone());
                high_map.insert(*w, h.clone());
            },
            _ => {},
        }
    }
    let mut bots : HashMap<(usize, usize), usize> = HashMap::new();
    for i in commands.iter() {
        match i {
            Instr::Value{which, to} => {
                add(&mut state, &Dest::Bot(*to), &which);
                run_state(&mut state, &mut bots, &low_map, &high_map);
            },
            _ => {},
        }
    }
    println!("{:?}", bots[&(17, 61)]);
    let outputs = vec![0, 1, 2];
    println!("{:?}", outputs.iter().flat_map(|i| state[&Dest::Output(*i)].clone()).fold(1, |a, b| a * b));
}
