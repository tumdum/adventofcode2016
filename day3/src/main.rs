use std::cmp::{min,max};
use std::io::stdin;
use std::io::Read;

fn is_triangle(a: i32, b: i32, c: i32) -> bool {
    let x = min(a, b);
    let tmp = max(a, b);
    let y = min(tmp, c);
    let z = max(tmp, c);
    (x+y) > z
}

fn main() {
    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    let mut tris : Vec<(i32,i32,i32)> = vec![];
    let mut count = 0;
    for line in buf.lines() {
        let mut numbers = line.split_whitespace();
        let a = numbers.next().unwrap().parse::<i32>().unwrap();
        let b = numbers.next().unwrap().parse::<i32>().unwrap();
        let c = numbers.next().unwrap().trim().parse::<i32>().unwrap();
        tris.push((a,b,c));
    }
    let mut i = 0;
    while i < tris.len() {
        if is_triangle(tris[i].0, tris[i+1].0, tris[i+2].0) {
            count += 1;
        }
        if is_triangle(tris[i].1, tris[i+1].1, tris[i+2].1) {
            count += 1;
        }
        if is_triangle(tris[i].2, tris[i+1].2, tris[i+2].2) {
            count += 1;
        }
        i += 3;
    }
    println!("count: {}", count);
}

#[test]
fn is_triangle_test() {
    assert!(is_triangle(2,3,4));
    assert!(is_triangle(2,4,3));
    assert!(is_triangle(4,3,2));
    assert!(is_triangle(4,2,3));
    assert!(is_triangle(3,2,4));
    assert!(is_triangle(3,4,2));
    assert!(!is_triangle(1,2,3));
}
