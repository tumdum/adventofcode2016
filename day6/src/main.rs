use std::collections::HashMap;
use std::io::stdin;
use std::io::Read;
use core::i32;

extern crate core;

fn find_max(f: &HashMap<char, i32>) -> char {
    let mut best = 0;
    let mut ret = ' ';
    for (c, i) in f {
        if i > &best {
            ret = *c;
            best = *i;
        }
    }
    ret
}

fn find_min(f: &HashMap<char, i32>) -> char {
    let mut best = core::i32::MAX;
    let mut ret = ' ';
    for (c, i) in f {
        if i < &best {
            ret = *c;
            best = *i;
        }
    }
    ret
}

fn main() {
    let mut freq = [HashMap::new(),
                    HashMap::new(),
                    HashMap::new(),
                    HashMap::new(),
                    HashMap::new(),
                    HashMap::new(),
                    HashMap::new(),
                    HashMap::new()];
    let mut buf = String::new();
    stdin().read_to_string(&mut buf).unwrap();
    for line in buf.lines() {
        for (i, c) in line.chars().enumerate() {
            let f = freq[i].entry(c).or_insert(0);
            *f += 1;
        }
    }
    for f in freq.iter() {
        print!("{}", find_min(&f));
    }
}
