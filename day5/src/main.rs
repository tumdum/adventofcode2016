extern crate crypto;
extern crate rustc_serialize;

use std::fmt::Write;
use std::collections::HashMap;
use crypto::md5::Md5;
use crypto::digest::Digest;
use rustc_serialize::hex::ToHex;

fn hash(h: &mut Md5, prefix: &str, num: i32) -> String {
    h.input(prefix.as_bytes());
    h.input(num.to_string().as_bytes());
    let mut res = [0; 16];
    h.result(&mut res);
    h.reset();
    res.to_hex()
}

fn main1() {
    let mut hasher = Md5::new();
    let input = "ffykfhsq";
    let mut i = 0;
    let mut found = 0;
    while found < 8 {
        let hex = hash(&mut hasher, input, i);
        if hex.bytes().take(5).all(|c| c == '0' as u8) {
            print!("{}", hex.chars().nth(5).unwrap());
            found += 1;
        }
        i += 1;
    }
}

fn parse(c: char) -> i32 {
    c as i32 - '0' as i32
}

fn main2() {
    let mut hasher = Md5::new();
    let input = "ffykfhsq";
    let mut i = 0;
    let mut result : HashMap<i32,char> = HashMap::new();
    while result.len() < 8 {
        let hex = hash(&mut hasher, input, i);
        if hex.bytes().take(5).all(|c| c == '0' as u8) {
            let ch  = hex.chars().nth(6).unwrap();
            let pos = parse(hex.chars().nth(5).unwrap());
            if pos >= 0 && pos <= 7 && !result.contains_key(&pos) {
                result.insert(pos, ch);
            }
        }
        i += 1;
    }
    for i in (0..8) {
        print!("{}", result[&i]);
    }
    println!("");
}

fn main() {
    main2();
}
