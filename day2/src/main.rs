use std::collections::HashMap;
use std::io::Read;

#[derive(Hash,Clone,PartialEq,Eq,Debug)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn step(&self, dir: char) -> Position {
        let mut copy = self.clone();
        match dir {
            'U' => copy.y += 1,
            'D' => copy.y -= 1,
            'L' => copy.x -= 1,
            'R' => copy.x += 1,
            _   => panic!("?"),
        }
        copy
    }

    fn make_move(&self, dir: &str, bounds: &HashMap<Position,char>) -> Position {
        let mut current = self.clone();
        for c in dir.chars() {
            let next = current.step(c);
            if bounds.contains_key(&next) {
                current = next;
            } 
        }
        current
    }
}

fn solve(start: &Position, keys: &HashMap<Position,char>, input: Vec<String>) {
    let mut current = start.clone();
    let mut key = "".to_string();
    for p in input {
        current = current.make_move(&p, keys);
        if let Some(&val) = keys.get(&current) {
            key.push_str(&val.to_string());
        }
    }
    println!("key: {}", key);
}

fn read_lines() -> Vec<String> {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let mut lines : Vec<String> = vec![];
    for line in buf.split_whitespace() {
        lines.push(line.to_string());
    }
    lines
}

fn main_first_part() {
    let mut codes : HashMap<Position,char>= HashMap::new();
    codes.insert(Position{x:0,y:0}, '1');
    codes.insert(Position{x:1,y:0}, '2');
    codes.insert(Position{x:2,y:0}, '3');
    codes.insert(Position{x:0,y:-1}, '4');
    codes.insert(Position{x:1,y:-1}, '5');
    codes.insert(Position{x:2,y:-1}, '6');
    codes.insert(Position{x:0,y:-2}, '7');
    codes.insert(Position{x:1,y:-2}, '8');
    codes.insert(Position{x:2,y:-2}, '9');

    let lines = read_lines();
    solve(&Position{x:1,y:-1}, &codes, lines);
}

fn main_second_part() {
    let mut codes : HashMap<Position,char>= HashMap::new();

    codes.insert(Position{x:2,y:2}, '1');
    codes.insert(Position{x:1,y:1}, '2');
    codes.insert(Position{x:2,y:1}, '3');
    codes.insert(Position{x:3,y:1}, '4');
    codes.insert(Position{x:0,y:0}, '5');
    codes.insert(Position{x:1,y:0}, '6');
    codes.insert(Position{x:2,y:0}, '7');
    codes.insert(Position{x:3,y:0}, '8');
    codes.insert(Position{x:4,y:0}, '9');
    codes.insert(Position{x:1,y:-1}, 'A');
    codes.insert(Position{x:2,y:-1}, 'B');
    codes.insert(Position{x:3,y:-1}, 'C');
    codes.insert(Position{x:2,y:-2}, 'D');

    let lines = read_lines();
    solve(&Position{x:0,y:0}, &codes, lines);
}

fn main() {
    // main_first_part();
    main_second_part();
}

#[test]
fn test_step() {
    assert_eq!(Position{x:0,y:0}, Position{x:1,y:-1}.step('U').step('L'));
}

#[test]
fn test_move() {
    let xb = (0,3);
    let yb = (-2,1);
    assert_eq!(Position{x:0,y:0}, Position{x:1,y:-1}.make_move("ULL",xb,yb));
    assert_eq!(Position{x:2,y:-2}, Position{x:0,y:0}.make_move("RRDDD",xb,yb));
    assert_eq!(Position{x:1,y:-2}, Position{x:2,y:-2}.make_move("LURDL",xb,yb));
    assert_eq!(Position{x:1,y:-1}, Position{x:1,y:-2}.make_move("UUUUD",xb,yb));
}
