#[macro_use]
extern crate lazy_static;

use std::io::Read;

lazy_static! {
    static ref MARKER_RE: regex::Regex =
        regex::Regex::new(r"\((\d+)x(\d+)\)").unwrap();
}

#[derive(Debug, PartialEq)]
struct Marker {
    len: usize,
    times: usize,
}

impl Marker {
    fn decompress<'a>(&self, input: &'a str) -> (usize, &'a str) {
        let prefix = &input[0..self.len];
        (prefix.len() * self.times, &input[self.len..])
    }

    fn decompress2<'a>(&self, input: &'a str) -> (usize, &'a str) {
        let prefix = &input[0..self.len];
        let dec = prefix.repeat(self.times);
        let dec = decompress_all2(&dec);
        (dec, &input[self.len..])
    }

    fn find_next(input: &str) -> Option<(usize, Marker, &str)> {
        let next_start = input.find('(')?;
        let prefix = &input[0..next_start];
        let rest = &input[next_start..];
        let next_end = rest.find(')')?;
        let marker = &rest[0..next_end+1];
        let rest = &rest[next_end+1..];
        Some((prefix.len(), marker.into(), rest))
    }
}

impl From<&str> for Marker {
    fn from(s: &str) -> Marker {
        let cap = MARKER_RE.captures(s).unwrap();
        Marker{
            len: cap[1].parse().unwrap(),
            times: cap[2].parse().unwrap(),
        }
    }
}

fn decompress_all(s: &str) -> usize {
    let mut len = 0;
    let mut current = s;
    loop {
        if let Some((prefix, marker, suffix)) = Marker::find_next(current) {
            len += prefix;
            let (decompressed, next) = marker.decompress(suffix);
            len += decompressed;
            current = next;
        } else {
            len += current.len();
            break;
        }
    }
    len
}

fn decompress_all2(s: &str) -> usize {
    let mut len = 0;
    let mut current = s;
    loop {
        if let Some((prefix, marker, suffix)) = Marker::find_next(current) {
            len += prefix;
            let (decompressed, next) = marker.decompress2(suffix);
            len += decompressed;
            current = next;
        } else {
            len += current.len();
            break;
        }
    }
    len
}

#[test]
fn marker_from() {
    assert_eq!(Marker{len: 10, times: 2}, "(10x2)".into());
}

#[test]
fn marker_decompress() {
    let result = Marker{len: 1, times: 5}.decompress("abc");
    assert_eq!(("aaaaa".len(), "bc"), result);
    let result = Marker{len: 3, times: 2}.decompress("abc");
    assert_eq!(("abcabc".len(), ""), result);
}

#[test]
fn marker_find_next() {
    assert_eq!(None, Marker::find_next("abcd"));
    assert_eq!(Some((3, Marker{len: 1, times: 2}, "def")),
        Marker::find_next("abc(1x2)def"));
}

#[test]
fn decompress() {
    assert_eq!("ADVENT".len(), decompress_all("ADVENT"));
    assert_eq!("ABBBBBC".len(), decompress_all("A(1x5)BC"));
    assert_eq!("XYZXYZXYZ".len(), decompress_all("(3x3)XYZ"));
    assert_eq!("ABCBCDEFEFG".len(), decompress_all("A(2x2)BCD(2x2)EFG"));
    assert_eq!("(1x3)A".len(), decompress_all("(6x1)(1x3)A"));
    assert_eq!("X(3x3)ABC(3x3)ABCY".len(), decompress_all("X(8x2)(3x3)ABCY"));
}

#[test]
fn decompress2() {
    assert_eq!("XABCABCABCABCABCABCY".len(),
        decompress_all2("X(8x2)(3x3)ABCY"));
    assert_eq!(445,
        decompress_all2("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"));
}

fn main() {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf).unwrap();
    let input : String = buf.chars().filter(|c| !c.is_whitespace()).collect();
    let decompressed = decompress_all(&input);
    println!("{}", decompressed);
    let decompressed = decompress_all2(&input);
    println!("{}", decompressed);
}
