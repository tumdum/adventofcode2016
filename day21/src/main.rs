#[macro_use]
extern crate lazy_static;

use std::io::BufRead;
use regex::Regex;
use std::ops::Deref;

lazy_static! {
    static ref SWAP_RE : Regex =
        Regex::new(r"swap position (\d+) with position (\d+)").unwrap();
    static ref SWAP_LET_RE : Regex =
        Regex::new(r"swap letter (.) with letter (.)").unwrap();
    static ref REVERSE_RE : Regex =
        Regex::new(r"reverse positions (\d+) through (\d+)").unwrap();
    static ref ROTATE_RE : Regex =
        Regex::new(r"rotate (.+) (\d+) step").unwrap();
    static ref MOVE_RE : Regex =
        Regex::new(r"move position (\d+) to position (\d+)").unwrap();
    static ref ROTATE_BASED_RE : Regex =
        Regex::new(r"rotate based on position of letter (.)").unwrap();
}

#[derive(Clone, Debug, PartialEq)]
enum Dir { Left, Right }

impl From<&str> for Dir {
    fn from(s: &str) -> Dir {
        match s {
            "left" => Dir::Left,
            "right" => Dir::Right,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, PartialEq)]
enum Op {
    SwapPos{x: usize, y: usize},
    SwapLet{x: char, y: char},
    Rotate{dir: Dir, x: usize},
    RotateBased{x: char, dir: Dir},
    Reverse{x: usize, y: usize},
    Move{x: usize, y: usize},
}

impl From<&str> for Op {
    fn from(s: &str) -> Op {
        if let Some(cap) = SWAP_RE.captures(s) {
            return Op::SwapPos{
                x: cap[1].parse().unwrap(),
                y: cap[2].parse().unwrap(),
            };
        }
        if let Some(cap) = SWAP_LET_RE.captures(s) {
            return Op::SwapLet{
                x: cap[1].chars().next().unwrap(),
                y: cap[2].chars().next().unwrap(),
            };
        }
        if let Some(cap) = REVERSE_RE.captures(s) {
            return Op::Reverse{
                x: cap[1].parse().unwrap(),
                y: cap[2].parse().unwrap(),
            };
        }
        if let Some(cap) = ROTATE_RE.captures(s) {
            return Op::Rotate{
                dir: cap[1].into(),
                x: cap[2].parse().unwrap(),
            };
        }
        if let Some(cap) = MOVE_RE.captures(s) {
            return Op::Move{
                x: cap[1].parse().unwrap(),
                y: cap[2].parse().unwrap(),
            };
        }
        if let Some(cap) = ROTATE_BASED_RE.captures(s) {
            return Op::RotateBased{
                x: cap[1].chars().next().unwrap(),
                dir: Dir::Right,
            };
        }
        unimplemented!()
    }
}

fn find(v: &[char], e: char) -> usize {
    v.iter().enumerate().filter(|(_,c)| **c == e).next().unwrap().0
}

fn execute_many(ops: &[Op], input: &str) -> String {
    let mut current = input.to_owned();
    for op in ops.iter() {
        current = execute(op, &current);
    }
    current
}

fn execute(op: &Op, input: &str) -> String {
    let mut arr : Vec<_> = input.chars().collect();
    match op {
        Op::SwapPos{x, y} => {
            let tmp : char = arr[*x].clone();
            arr[*x] = arr[*y].clone();
            arr[*y] = tmp;
            arr.into_iter().collect()
        },
        Op::SwapLet{x, y} => {
            arr.into_iter()
                .map(|c| if c == *x { *y } else if c == *y { *x } else { c })
                .collect()
        },
        Op::Reverse{x, y} => {
            arr[*x..=*y].reverse();
            arr.into_iter().collect()
        },
        Op::Rotate{dir, x} => {
            let l = arr.len();
            match dir {
                Dir::Left => arr.rotate_left(*x % l),
                Dir::Right => arr.rotate_right(*x % l),
            }
            arr.into_iter().collect()
        },
        Op::Move{x, y} => {
            let tmp = arr.remove(*x);
            arr.insert(*y, tmp);
            arr.into_iter().collect()
        },
        Op::RotateBased{x, dir} => {
            let idx = find(&arr, *x);
            let right_shift = 1 + idx + if idx >= 4 { 1 } else { 0 };
            execute(&Op::Rotate{dir: dir.clone(), x: right_shift}, input)
        },
    }
}

fn all_perms(s: &str) -> Vec<String> {
    use permutohedron::Heap;

    let mut data : Vec<char> = s.chars().collect();
    let heap = Heap::new(&mut data);

    let mut permutations = Vec::new();
    for data in heap {
        permutations.push(data.iter().cloned().collect());
    }
    permutations
}

fn main() {
    let swappos = Op::SwapPos{x: 4, y: 0};
    assert_eq!("ebcda", execute(&swappos, "abcde"));

    let swaplet = Op::SwapLet{x: 'd', y: 'b'};
    assert_eq!("edcba", execute(&swaplet, "ebcda"));

    let reverse = Op::Reverse{x: 0, y: 4};
    assert_eq!("abcde", execute(&reverse, "edcba"));

    let rot = Op::Rotate{dir: Dir::Left, x: 1};
    assert_eq!("bcdea", execute(&rot, "abcde"));

    let mov = Op::Move{x: 1, y: 4};
    assert_eq!("bdeac", execute(&mov, "bcdea"));

    assert_eq!("abdec", execute(&Op::Move{x: 3, y: 0}, "bdeac"));

    let rotb = Op::RotateBased{x: 'b', dir: Dir::Right};
    assert_eq!("ecabd", execute(&rotb, "abdec"));

    assert_eq!("decab", execute(&Op::RotateBased{x: 'd', dir: Dir::Right}, "ecabd"));

    assert_eq!(Op::SwapPos{x: 4, y: 0}, "swap position 4 with position 0".into());
    assert_eq!(Op::SwapLet{x: 'd', y: 'b'}, "swap letter d with letter b".into());
    assert_eq!(Op::Reverse{x: 0, y: 4}, "reverse positions 0 through 4".into());
    assert_eq!(Op::Rotate{dir: Dir::Left, x: 1}, "rotate left 1 step".into());
    assert_eq!(Op::Move{x: 1, y: 4}, "move position 1 to position 4".into());
    assert_eq!(Op::RotateBased{x: 'b', dir: Dir::Right}, "rotate based on position of letter b".into());
    
    let ops : Vec<Op> = std::io::BufReader::new(std::io::stdin())
        .lines()
        .map(|s| s.unwrap().deref().into())
        .collect();
    println!("{}", execute_many(&ops,"abcdefgh"));

    let scrambled = "fbgdceah";
    let perms = all_perms(&scrambled);
    for perm in perms.iter() {
        let result = execute_many(&ops, perm);
        if result == scrambled {
            println!("{}", perm);
            break;
        }
    }
}
